Delta=350 #До какой степени сжимать

if [ -z ${1+x} ]; then
	ListFile=`ls | grep .jpg | grep  -v "_thumbnail"` #Перебираю все файлы в каталоге и отпрасываю лишнее
	MassiveListFile=($(echo $ListFile | tr " " "\n")) #Трансформирую строку в массив
else 
	readarray MassiveListFile < $1 
fi


for Picture in ${MassiveListFile[@]}
	do
	#Меряю длинну и ширину картинки
	HeightPic=$(identify -format '%h' $Picture)
	WidthPic=$(identify -format '%w' $Picture)
	#Формирую новое имя файла
	NamePicNotExtension=`echo $Picture  | sed 's/.jpg//'`
	NewNamePic=${NamePicNotExtension}_thumbnail.jpg
	#Сравниваю что больше и преобразую картинку
	if (($HeightPic > $WidthPic)); then
		convert $Picture -resize ${Delta}x${WidthPic}! $NewNamePic
	else 
		convert $Picture -resize ${HeightPic}x${Delta}! $NewNamePic
	fi
done

echo "Изображения успешно преобразованы"	


#Выполнение заняло чуть больше 3х часов.
#По большому счету первый мой скрипт, потому так долго, гуглить приходилось вообще все, даже то как оформляются тут переменные, циклы, условия и т.д.
#Тут нет никаких проверок на корректность ввода параметра или корректность заполнения файла, который туда передается
#По хорошему разумеется нужно все это разбить на функции (а верней изначально писать ими), но уже нет времени гуглить как это делается в bash.
#Если необходимо довести до ума (выше перечисленное), то уже нужно мне больше времени, хотя бы еще один такой вечер.
#Ну как то так 

